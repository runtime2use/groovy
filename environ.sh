#!/bin/bash

export CLASSPATH="$CLASSPATH;$LANG_PATH/dist"

################################################################################

ronin_require_groovy () {
    echo hello world >/dev/null
}

ronin_include_groovy () {
    motd_text "    -> Groovy    : "$CLASSPATH
}

################################################################################

ronin_setup_groovy () {
    echo hello world >/dev/null
}

